参考：
http://stackoverflow.com/questions/14450426/ios-creating-a-reusable-view-woes
http://onedayitwillmake.com/blog/2013/07/ios-creating-reusable-uiviews-with-storyboard/

其他复杂的：
http://cocoanuts.mobi/2014/03/26/reusable/
http://blog.yangmeyer.de/blog/2012/07/09/an-update-on-nested-nib-loading
例子：
https://github.com/yangmeyer/YMCalendarSheet

简述：
1. 建一个 UIView的子类(MyView.h/MyView.m)
2. 建一个 View类型的XIB
3. 把xib的file‘s owner设为MyView
4. 在.h文件里加上
@property (nonatomic, retain) IBOutlet UIView *contentView;

5.绑定xib里的根view到.h里的 contentView
6.在.m里加上

- (void)awakeFromNib
{
    NSLog(@"awake from nib");
    [[NSBundle mainBundle] loadNibNamed:@"MyView" owner:self options:nil];
    [self addSubview:self.contentView];
}
ok了。storyboard里用的时候把UIView的类名改为MyView就可以。xib和.h可以互相绑定子outlet什么的。
我的例程：
https://bitbucket.org/fangj/reusexibviewinstoryboardexample