//
//  main.m
//  testViewFromXib
//
//  Created by Fang Jian on 14-9-13.
//  Copyright (c) 2014年 Jian Fang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
