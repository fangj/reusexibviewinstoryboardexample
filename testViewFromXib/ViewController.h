//
//  ViewController.h
//  testViewFromXib
//
//  Created by Fang Jian on 14-9-13.
//  Copyright (c) 2014年 Jian Fang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyView.h"
@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet MyView *myView;

@end
